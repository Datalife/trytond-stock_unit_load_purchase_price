=======================================
Stock Unit Load Purchase Price Scenario
=======================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta


Install stock_unit_load_purchase_price::

    >>> config = activate_modules('stock_unit_load_purchase_price')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])


Create UdC::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.product = template.products[0]
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('35.0')
    >>> unit_load.production_location = production_loc
    >>> unit_load.purchase_price = Decimal('2.0')
    >>> unit_load.purchase_amount
    Decimal('70.00')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = storage_loc
    >>> unit_load.save()
    >>> unit_load = UnitLoad(unit_load.id)
    >>> unit_load.purchase_amount
    Decimal('70.00')